//
//  Colors+Ext.swift
//  BaseProject
//
//  Created by Qais Alnammari on 6/24/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit

extension UIColor {
    
    class var customBlue:UIColor {
       return UIColor(red: 82/255, green: 181/255, blue: 217/255, alpha: 1)
    }
    
    class var customDarkBlue: UIColor {
        return UIColor(red: 88/255, green: 166/255, blue: 189/255, alpha: 1)
    }
    
    class var customGray: UIColor {
        return UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1)
    }
    
    class var babyGreen: UIColor {
        return UIColor(red: 124/255, green: 219/255, blue: 170/255, alpha: 1)
    }
    
    class var babyOrange: UIColor {
        return UIColor(red: 237/255, green: 126/255, blue: 98/255, alpha: 1)
    }
    
    class var babyPurple: UIColor {
        return UIColor(red: 153/255, green: 122/255, blue: 193/255, alpha: 1)
    }
    
    class var babyYellow: UIColor {
        return UIColor(red: 242/255, green: 209/255, blue: 83/255, alpha: 1)
    }
    
    class var babyRed: UIColor {
        return UIColor(red: 225/255, green: 106/255, blue: 120/255, alpha: 1)
    }
    
    
    
    public convenience init?(hexString: String) {
        let r, g, b, a: CGFloat
        
        var hexColor = hexString
        if hexString.hasPrefix("#") {
            let start = hexString.index(hexString.startIndex, offsetBy: 1)
            hexColor = String(hexString[start...])
        }
        while hexColor.count < 8{
            hexColor += "f"
        }
        
        if hexColor.count == 8 {
            let scanner = Scanner(string: hexColor)
            var hexNumber: UInt64 = 0
            
            if scanner.scanHexInt64(&hexNumber) {
                r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    self.init(red: r, green: g, blue: b, alpha: a)
                return
            }
        }
        self.init(white: 1, alpha: 1)
    }
    
}
