//
//  Font+Ext.swift
//  BaseProject
//
//  Created by Qais Alnammari on 6/26/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit

class AppFonts {
    
    enum Weight {
        case plain
        case bold
        case black
        
        var familyName:String {
            switch self {
            case .plain:
                return "TheSansArabic-Plain"
            case .bold:
                return "TheSansArabic-Bold"
            case .black:
                return "TheSansArabic-Black"
            }
        }
    }
    
    class func font(forWeight weight:Weight,size:CGFloat) -> UIFont {
        guard let localizedFont = UIFont(name: weight.familyName, size: size) else {
            print("Error : Couldnt find the custom font")
            return UIFont.systemFont(ofSize: size)
        }
        return localizedFont
    }
}
