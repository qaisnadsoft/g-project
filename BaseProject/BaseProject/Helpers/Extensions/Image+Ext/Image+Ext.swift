//
//  Image+Ext.swift
//  BaseProject
//
//  Created by Qais Alnammari on 6/24/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit

extension UIImage {
    
    class func imgWithBackgroundColor(color:UIColor,size:CGSize) -> UIImage {
        var rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if UIScreen.main.bounds.size.height > 740 { // iphone X
             rect = CGRect(x: 0, y: 30, width: size.width, height: size.height)
            
        }
        return img ?? UIImage()
    }
}
