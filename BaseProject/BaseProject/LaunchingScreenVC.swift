//
//  LaunchingScreenVC.swift
//  BaseProject
//
//  Created by Qais Alnammari on 6/23/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit

class LaunchingScreenVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupLounchingScreen()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func setupLounchingScreen() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2)  { [weak self] in
            
            guard let self = self else {
              return
            }
            
            let vc = UIStoryboard(name: Routing.Storyboards.main.rawValue , bundle: nil).instantiateViewController(withIdentifier: Routing.VCsStoryboardsIds.homeID.rawValue)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
