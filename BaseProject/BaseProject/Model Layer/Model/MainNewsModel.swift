//
//  MainNewsModel.swift
//  BaseProject
//
//  Created by Qais Alnammari on 6/23/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import Foundation

class MainNewsModel: Codable {
    
    var catName:String
    var catID:String
    var catDesign:String
    var color:String?
    var picture:String?
    var bigAD :String?
    var articles:[ArticalModel]?
    var ads:AdsModel?
    
    
    private enum CodingKeys:String,CodingKey {
        case catName = "CatName"
        case catID = "CatID"
        case catDesign = "CatDesign"
        case articles = "Articles"
        case ads = "Ads"
        case color = "Color"
        case picture = "Picture"
        case bigAD = "BigAD"
    }
    
}

class ArticalModel: Codable {
    var articleID:String
    var datetime:String
    var title:String
    var image:String
    var intro:String
    var category:String
    var externalLink:String
    var type:String
//    var typeID:Int
    
    private enum CodingKeys:String,CodingKey {
        case articleID = "ArticleID"
        case datetime = "Datetime"
        case title = "Title"
        case image = "Image"
        case intro = "Intro"
        case category = "Category"
        case externalLink = "ExternalLink"
        case type = "Type"
//        case typeID = "TypeID"
    }
    
}

class AdsModel:Codable {
    var type:String
    var typeID:Int
    var artAD:String
    
    private enum CodingKeys:String,CodingKey {
        case type = "Type"
        case typeID = "TypeID"
        case artAD = "ArtAD"
    }
}
