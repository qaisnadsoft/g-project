//
//  SideMenuModel.swift
//  BaseProject
//
//  Created by Qais Alnammari on 6/24/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import Foundation

class SideMenuModel: Codable {
    
    var catName : String
    var catID : String
    var color:String
    var catDesign: String
    var picture:String
    var bigAD:String?
    var hideAfter:String?
    
    private enum CodingKeys:String,CodingKey {
        case catName = "CatName"
        case catID = "CatID"
        case color = "Color"
        case catDesign = "CatDesign"
        case picture = "Picture"
        case bigAD = "BigAD"
        case hideAfter = "HideAfter"
    }
    
}
