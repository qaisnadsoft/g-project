//
//  DataSource.swift
//  BaseProject
//
//  Created by Qais Alnammari on 2/11/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import Foundation

final class DataSource : DataSourceProtocol {
    
    static var shared: DataSourceProtocol = DataSource()
    
    lazy var remoteDataSource:DataSourceProtocol = RemoteDataSource()
    lazy var localDataSource = LocalDataSource()
    
    func getNews(type: String, success: (([MainNewsModel]) -> Void)?, failure: ((NSError) -> Void)?) {
        remoteDataSource.getNews(type: type, success: { (news) in
            success?(news)
        }, failure: failure)
    }
    
    func getSideMenuItems(type: String, success: (([SideMenuModel]) -> Void)?, failure: ((NSError) -> Void)?) {
        remoteDataSource.getSideMenuItems(type: type, success: { (items) in
            success?(items)
        }, failure: failure)
    }
    
    func getNewsDetails(type: String, catId: String, page: Int, success: (([MainNewsModel]) -> Void)?, failure: ((NSError) -> Void)?) {
        remoteDataSource.getNewsDetails(type: type, catId: catId, page: page, success: { (items) in
            success?(items)
        }, failure: failure)
    }
}
