//
//  DataSourceProtocol.swift
//  BaseProject
//
//  Created by Qais Alnammari on 2/11/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import Foundation

protocol DataSourceProtocol {
    
    func getNews(type:String,success:(([MainNewsModel])->Void)? , failure:((NSError)->Void)?)
    func getSideMenuItems(type:String,success:(([SideMenuModel])->Void)? , failure:((NSError)->Void)?)
    func getNewsDetails(type:String,catId:String,page:Int,success:(([MainNewsModel])->Void)? , failure:((NSError)->Void)?)
    
}
