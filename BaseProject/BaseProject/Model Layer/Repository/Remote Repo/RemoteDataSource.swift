//
//  RemoteDataSource.swift
//  BaseProject
//
//  Created by Qais Alnammari on 2/11/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import Foundation
import Alamofire

class RemoteDataSource : DataSourceProtocol {
    
    //MARK:-Proparties
    lazy var remoteContext = RemoteContext(baseURL: ApiEndPoints.baseUrl)
    
    
    func getNews(type: String, success: (([MainNewsModel]) -> Void)?, failure: ((NSError) -> Void)?) {
        
        let url = ApiEndPoints.getNews.rawValue
        let endPoint = EndPoint(address: url, httpMethod: .get)
        let prams = [
            "mod":type
        ]
        
        remoteContext.request(endPoint: endPoint, parameters: prams) { (result, data) in
            guard result else {
                //Failure
                let error = data as? NSError
                print("Error occured while authenticating user. In \(#function). \(error!.localizedDescription)")
                failure?(error!)
                return
            }
            
            
            let decoder = JSONDecoder()
            
            do{
                let model = try decoder.decode([MainNewsModel].self, from: data as! Data)
                success?(model)
            }catch let err{
                failure?(err as NSError)
            }
            
        }
    }
    
    func getSideMenuItems(type: String, success: (([SideMenuModel]) -> Void)?, failure: ((NSError) -> Void)?) {
        
        let url = ApiEndPoints.getNews.rawValue
        let endPoint = EndPoint(address: url, httpMethod: .get)
        let prams = [
            "mod":type
        ]
        
        remoteContext.request(endPoint: endPoint, parameters: prams) { (result, data) in
            guard result else {
                //Failure
                let error = data as? NSError
                print("Error occured while authenticating user. In \(#function). \(error!.localizedDescription)")
                failure?(error!)
                return
            }
            
            let decoder = JSONDecoder()
            
            do{
                let model = try decoder.decode([SideMenuModel].self, from: data as! Data)
                success?(model)
            }catch let err{
                failure?(err as NSError)
            }
            
        }
    }
    
    func getNewsDetails(type: String, catId: String, page: Int, success: (([MainNewsModel]) -> Void)?, failure: ((NSError) -> Void)?) {
        
        let url = ApiEndPoints.getNews.rawValue 
        let endPoint = EndPoint(address: url, httpMethod: .get)
        let prams = [
            "mod" : type,
            "CatID" : catId,
            "page" : page
            
            ] as [String : Any]
        
        remoteContext.request(endPoint: endPoint, parameters: prams) { (result, data) in
            guard result else {
                //Failure
                let error = data as? NSError
                print("Error occured while authenticating user. In \(#function). \(error!.localizedDescription)")
                failure?(error!)
                return
            }
            
            
            let decoder = JSONDecoder()
            
            do{
                let model = try decoder.decode([MainNewsModel].self, from: data as! Data)
                success?(model)
            }catch let err{
                failure?(err as NSError)
            }
            
        }

        
        
        
    }
    
}
    
    

