//
//  Routing.swift
//  BaseProject
//
//  Created by Qais Alnammari on 6/23/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import Foundation

struct Routing {
    enum Storyboards:String {
        case main = "Main"
        case newsDetails = "NewsDetails"
    }
    
    enum VCsStoryboardsIds:String {
        case mainVC = "MainVC"
        case homeID = "HomeID"
        case sideMenuVC = "SideMenuVC"
        case newsDetailsVC = "NewsDetailsVC"
    }
}
