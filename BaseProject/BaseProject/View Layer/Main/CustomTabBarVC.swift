//
//  CustomTabBarVC.swift
//  BaseProject
//
//  Created by Qais Alnammari on 6/24/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit

class CustomTabBarVC: UITabBarController {

    //Variables:-
    var tabBarIteam = UITabBarItem()
    var imgNames = ["slices-3","slices-4","slices-5","slices-6"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setupTabBar()
    }
    
    
    func setupTabBar(){
        
        UITabBar.appearance().barTintColor = UIColor.customBlue
        
        for item in 0...3 {
            let img = UIImage(named: imgNames[item])?.withRenderingMode(.alwaysTemplate)
            
            guard let tempItem = self.tabBar.items else {
                return
            }
            
            tabBarIteam = tempItem[item]
            tabBarIteam.image = img
            tabBarIteam.selectedImage = img?.withRenderingMode(.alwaysOriginal)
            
        }
        
        self.selectedIndex = 0
        
//        let numberOfTabs = CGFloat((self.tabBar.items?.count) ?? 0)
//        let tabBarSize = CGSize(width: tabBar.frame.width / numberOfTabs, height: tabBar.bounds.size.height)
//        let selectedColor = UIColor.customDarkBlue
//        tabBar.selectionIndicatorImage = UIImage.imgWithBackgroundColor(color: selectedColor, size: tabBarSize)
//        solve iphone x problem
    }
   
}
