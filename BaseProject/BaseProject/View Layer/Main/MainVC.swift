//
//  MainVC.swift
//  BaseProject
//
//  Created by Qais Alnammari on 2/3/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit
import Toast_Swift
import WebKit
import Kingfisher

class MainVC: UIViewController {
    
   
    
    //Outlets:-
    @IBOutlet weak var subNewsTableView: UITableView!
    
    //Variables:-
    
    private var viewModel = MainViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        configuerUI()
        configuerData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
        
    }
    
    func configuerUI() {
        setupTableView()
    }
    
    func configuerData(){
        _ = SKPreloadingView.show()
        viewModel.getNews(type: "main", success: { [weak self] in
            
            guard let self = self else {
                return
            }
            
            self.subNewsTableView.reloadData()
            
            SKPreloadingView.hide()
        }) { [weak self] (err)  in
            
            guard let self = self else {
                return
            }
            self.view.makeToast(err.localizedDescription)
            SKPreloadingView.hide()
            print(err)
        }
    }
    
    func setupTableView() {
        
        subNewsTableView.register(PagerViewCell.self)
        subNewsTableView.register(CategoryTitleCell.self)
        subNewsTableView.register(SubNewsCell.self)
        
        subNewsTableView.delegate = self
        subNewsTableView.dataSource = self
    }
    
}

extension MainVC : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.newsCout
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            return 0
        }
        
        return 101
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section != 0 {
            let cell = subNewsTableView.dequeueReusableCell(withIdentifier: "CategoryTitleCell") as? CategoryTitleCell
            let info = viewModel.news[section]
            if let url = URL(string: info.ads?.artAD ?? "") {
                let urlRequest = URLRequest(url: url)
                cell?.webView.load(urlRequest)
            }
            
            cell?.titleName.text = info.catName
            if let color = info.color {
                cell?.titleName.backgroundColor = UIColor.init(hexString: color)
            }
            return cell
        }
        return UIView()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        
        let rowsCount = viewModel.news[section].articles?.count ?? 0
        if rowsCount < 3 {
            return viewModel.news[section].articles?.count ?? 0
        } else {
            return 3
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let artical = viewModel.news[indexPath.section].articles?[indexPath.row]
        let news = viewModel.news[indexPath.section]
        if indexPath.section == 0 {
            let cell = subNewsTableView.dequeueReusableCell(forIndexPath: indexPath) as PagerViewCell
            cell.selectionStyle = .none
            
            if let articals = news.articles {
                cell.articals = articals
                cell.pageController.numberOfPages = articals.count
            }
            
            return cell
        } else {
            let cell = subNewsTableView.dequeueReusableCell(forIndexPath: indexPath) as SubNewsCell
            
            
            cell.selectionStyle = .none
            cell.newsTitle.text = artical?.title
            
            let url = URL(string: artical?.image ?? "")
            cell.newsImg.kf.setImage(with: url, placeholder: UIImage(named: "placeHolder"))
            cell.newsImg.contentMode = .scaleAspectFill
            
            if indexPath.row % 2 != 0 {
                cell.mainView.backgroundColor = UIColor.customGray
            } else {
                cell.mainView.backgroundColor = UIColor.white
            }
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 230
        }
        return 80
    }
}

