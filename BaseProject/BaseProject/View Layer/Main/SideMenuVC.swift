//
//  SideMenuVC.swift
//  BaseProject
//
//  Created by Qais Alnammari on 6/25/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit
import SideMenu
import Toast_Swift
import Kingfisher

class SideMenuVC: UIViewController {
    
    //Outlets:-
    @IBOutlet weak var sideMenuCollectionView: UICollectionView!
    //Variables:-
    private var viewModel = SideMenuViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configuerUI()
        configuerData()
    }
    
    func configuerUI() {
        self.navigationController?.isNavigationBarHidden = true
        setupCollectionView()
        setupSideMenu()
        
    }
    
    func configuerData() {
        _ = SKPreloadingView.show()
        viewModel.getMeniItems(success: { [weak self] in
            guard let self = self else {
                return
            }
            
            self.sideMenuCollectionView.reloadData()
            SKPreloadingView.hide()
        }) { [weak self] (err) in
            guard let self = self else {
                return
            }
            
            SKPreloadingView.hide()
            self.view.makeToast(err.localizedDescription)
        }
    }
    
    func setupCollectionView() {
        sideMenuCollectionView.register(SideMenuCell.self)
        sideMenuCollectionView.delegate = self
        sideMenuCollectionView.dataSource = self
    }
    
    func setupSideMenu() {
        
        let width = (self.view.bounds.size.width) * 0.80
        SideMenuManager.default.menuWidth = width
        
    }
    
}

extension SideMenuVC:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.itemsCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = sideMenuCollectionView.dequeueReusableCell(forIndexPath: indexPath) as SideMenuCell
        let info = viewModel.sideMenuItems[indexPath.item]
        let url = URL(string: info.picture)

        cell.cellIcon.kf.setImage(with: url, placeholder: UIImage(named: "placeHolder"))
        cell.cellTitle.text = info.catName
        cell.mainView.backgroundColor = UIColor(hexString: info.color)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var height:CGFloat = 0
        if viewModel.itemsCount % 2 == 0 {
            height = collectionView.bounds.size.height / (CGFloat(viewModel.itemsCount/2))
        } else {
            height = collectionView.bounds.size.height / (CGFloat((viewModel.itemsCount+1)/2))
        }
        
        let width = collectionView.bounds.size.width / 2
        return CGSize(width: width, height: height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let vc = UIStoryboard(name: Routing.Storyboards.newsDetails.rawValue , bundle: nil).instantiateViewController(withIdentifier: Routing.VCsStoryboardsIds.newsDetailsVC.rawValue) as? NewsDetailsVC {
            let catId = viewModel.sideMenuItems[indexPath.row].catID
            vc.viewModel.catId = catId
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}


