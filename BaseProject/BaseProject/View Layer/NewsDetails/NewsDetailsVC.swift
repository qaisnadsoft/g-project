//
//  NewsDetailsVC.swift
//  BaseProject
//
//  Created by Qais Alnammari on 6/26/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit
import Kingfisher

class NewsDetailsVC: UIViewController {
    //Outlets:-
    @IBOutlet weak var detailsTableView: UITableView!
    
    //Variables:-
    let viewModel = NewsDetailsViewModel()
    var isLoadingData:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configuerUI()
        configuerData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
        let imgView = UIImageView()
        imgView.contentMode = .scaleAspectFit
        imgView.image = UIImage(named: "slices-1")
        
        self.navigationItem.titleView?.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        self.navigationItem.titleView = imgView
        
        
       
        
    }
    
    func configuerUI() {
        setupTableView()
    }
    
    func configuerData() {
        let spinner = UIActivityIndicatorView(style: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: detailsTableView.bounds.width, height: 44)
        self.detailsTableView.tableFooterView = spinner
        self.detailsTableView.tableFooterView?.isHidden = false
        self.isLoadingData = true
        viewModel.getNewsArticals(success: { [weak self] in
            guard let self = self else {
                return
            }
            self.isLoadingData = false
            self.detailsTableView.reloadData()
            spinner.stopAnimating()
        }) { [weak self] (err) in
            guard let self = self else {
                return
            }
            spinner.stopAnimating()
            self.isLoadingData = false
            self.view.makeToast(err.localizedDescription)
        }
        
    }
    
    func setupTableView() {
        detailsTableView.register(ImageCell.self)
        detailsTableView.register(LableCell.self)
        detailsTableView.delegate = self
        detailsTableView.dataSource = self
    }
    
    @objc func shareNews(_ sender:UIButton) {
        print("Share Button Pressed")
    }
    
}

extension NewsDetailsVC:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.articalsCout
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = detailsTableView.dequeueReusableCell(forIndexPath: indexPath) as ImageCell
        cell.shareBtn.addTarget(self, action: #selector(shareNews), for: .touchUpInside)
        
        let info = viewModel.articals[indexPath.row]
        let url = URL(string: info.image)
        cell.cellImg.kf.setImage(with: url)
        cell.cellImg.contentMode = .scaleAspectFill
        
        cell.cellTitle.text = info.title
        cell.cellTitle.font = AppFonts.font(forWeight: .bold, size: 20)
        cell.cellTitle.numberOfLines = 2
        
        cell.cellDate.text = info.datetime
        cell.cellDate.font = AppFonts.font(forWeight: .plain, size: 14)
        
        cell.cellBody.text = info.intro
        cell.cellBody.numberOfLines = 0
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        let currentOfsetY = scrollView.contentOffset.y
//        let contentHeight = scrollView.contentSize.height
//        if (currentOfsetY + detailsTableView.bounds.size.height > contentHeight - 50) && !isLoadingData {
//            viewModel.page = viewModel.page + 1
//            //New request
//            configuerData()
//        }
//    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == 17 && !isLoadingData {
            if !viewModel.endOfPaging {
                configuerData()
            }
        }
    }
    
}
