//
//  MainViewModel.swift
//  BaseProject
//
//  Created by Qais Alnammari on 2/11/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import Foundation

class MainViewModel {
    
    var news = [MainNewsModel]()
    var newsCout:Int {
        return news.count
    }
    func getNews(type:String,success: @escaping ()->Void,failure: @escaping (NSError)->Void)  {
        DataSource.shared.getNews(type: type, success: { (news) in
            self.news = news
            success()
        }, failure: failure)
    }
}
