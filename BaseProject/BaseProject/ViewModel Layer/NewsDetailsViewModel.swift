//
//  NewsDetailsViewModel.swift
//  BaseProject
//
//  Created by Qais Alnammari on 6/26/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import Foundation

class NewsDetailsViewModel  {
    
    var page = 10
    var catId = ""
    var type = "cat"
    var articals = [ArticalModel]()
    var endOfPaging = false
    var articalsCout:Int {
        return articals.count
    }
    
    func getNewsArticals(success:@escaping ()->Void,failure:@escaping (NSError)->Void) {
        DataSource.shared.getNewsDetails(type: type, catId: catId, page: page, success: { (news) in
            
            guard let articals = news[0].articles else { // the object here return 1 at index 0
                return
            }
            
            self.articals.append(contentsOf: articals)
            
            if articals.count < 20 {
                self.endOfPaging = true
            }
            success()
        }, failure: failure)
    }
}
