//
//  SideMenuViewModel.swift
//  BaseProject
//
//  Created by Qais Alnammari on 6/25/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import Foundation

class SideMenuViewModel {
    
    var sideMenuItems = [SideMenuModel]()
    
    var itemsCount:Int {
        return sideMenuItems.count
    }
    
    func getMeniItems(success: @escaping ()->Void,failure: @escaping (NSError)->Void) {
        DataSource.shared.getSideMenuItems(type: "getCats", success: { (items) in
            self.sideMenuItems = items
            success()
        }, failure: failure)
    }
}
