//
//  PagerViewCustomCell.swift
//  BaseProject
//
//  Created by Qais Alnammari on 6/24/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit
import FSPagerView

class PagerViewCustomCell: FSPagerViewCell {

    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var cellTitle: EdgeInsetLabel!
    @IBOutlet weak var cellImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
