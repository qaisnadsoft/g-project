//
//  SideMenuCell.swift
//  BaseProject
//
//  Created by Qais Alnammari on 6/25/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit

class SideMenuCell: UICollectionViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
