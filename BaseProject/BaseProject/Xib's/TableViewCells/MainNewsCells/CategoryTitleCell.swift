//
//  CategoryTitleCell.swift
//  BaseProject
//
//  Created by Qais Alnammari on 6/25/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit
import WebKit
class CategoryTitleCell: UITableViewCell {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var titleName: EdgeInsetLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
