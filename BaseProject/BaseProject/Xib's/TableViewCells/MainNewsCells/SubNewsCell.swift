//
//  SubNewsCell.swift
//  BaseProject
//
//  Created by Qais Alnammari on 6/25/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit

class SubNewsCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var newsImg: UIImageView!
    @IBOutlet weak var newsTitle: EdgeInsetLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
