//
//  PagerViewCell.swift
//  BaseProject
//
//  Created by Qais Alnammari on 6/24/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit
import FSPagerView
import Kingfisher
class PagerViewCell: UITableViewCell {
    
    var articals = [ArticalModel]()
    
    @IBOutlet weak var pagerView: FSPagerView! 

    
    @IBOutlet weak var pageController: FSPageControl! {
        didSet {
            self.pageController.contentHorizontalAlignment = .center
            self.pageController.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
            self.pageController.hidesForSinglePage = true
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupPagerView()
        setupPageController()
    }
    
    func setupPagerView() {
        pagerView.delegate = self
        pagerView.dataSource = self
        pagerView.automaticSlidingInterval = 4.0
        pagerView.isInfinite = true
        
        let nibName = UINib(nibName: "PagerViewCustomCell", bundle: nil)
        pagerView.register(nibName, forCellWithReuseIdentifier: "PagerViewCustomCell")
        
    }
    
    func setupPageController() {
        
        self.pageController.setFillColor(UIColor.customBlue, for: .selected)
        self.pageController.setStrokeColor(.white, for: .selected)
        self.pageController.setFillColor(UIColor.white, for: .normal)
        
    }
    
}

extension PagerViewCell : FSPagerViewDataSource,FSPagerViewDelegate {
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return articals.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        if let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "PagerViewCustomCell", at: index) as? PagerViewCustomCell {
            
            let info = articals[index]
            let url = URL(string: info.image)
            cell.cellImg?.kf.setImage(with: url, placeholder: UIImage(named: "placeHolder"))
            cell.cellImg?.contentMode = .scaleAspectFill
            cell.cellTitle.text = info.title
            cell.cellTitle.textAlignment = .center
            cell.titleView.applyGradient(colors: [UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0),UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)])
            return cell
        }
      
        

        return FSPagerViewCell()
        
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageController.currentPage = targetIndex
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.pageController.currentPage = pagerView.currentIndex
    }
    
}
